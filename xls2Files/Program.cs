﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using System.Xml;

//TODO : check student IDs for duplication (use sets or dictionary), and throw error if found 
//       (currenlty we must handle it from outside by removing the xls file duplicates from Excel)
//TODO : Do full directory structure copying of tests folders instead of hard coding to "kern" folder only
//TODO : Remove the "Student ID" hard coded sheet column name and make it configurable from the .xml config file
//TODO : Portability to other platforms: use platform-independent folder separator instead of windows-specific "\\"

namespace xls2Files
{
    class Program
    {
        static void Main(string[] args)
        {
            string configFileName = "xls2Files.xml";

            if (args.Length == 0 && (File.Exists(configFileName) == false))
            {
                Console.WriteLine("Usage: xls2files <config_file.xml>");
                MessageBox.Show("Please make sure that xls2Files.xml exists, Or drag a \".xml\" configuration and drop it to xls2Files.exe", "Configuration not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (args.Length == 1)
            {
                configFileName = args[0];
            }

            if (File.Exists(configFileName) == false)
            {
                MessageBox.Show("Please make sure that configuration file " + configFileName + " exists in this directory!", "Configuration not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Console.WriteLine("\nConfiguration file = {0}", configFileName);


            // Read the config file.
            //
            string xlsFileName = "";
            string workFolder = ""; //must have both question_template and tests/<problems> folders
            string destFolder = "";
            List<string> problemsNames = new List<string>();
            string sourceSheet = "";
            string problem_file_column_name = "";
            string problem_file_path_name = "";

            try
            {
                XmlNode xmlNode;                
                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.Load(configFileName);

                //--------------------

                xmlNode = xmlDoc.DocumentElement.SelectSingleNode("xlsFileName");
                xlsFileName = xmlNode.Attributes.GetNamedItem("name").Value;

                if (File.Exists(xlsFileName) == false)
                {
                    MessageBox.Show("Source xls file not found, please make sure xls file exists beside xls2Files.exe", "xls File not found");
                    return;
                }

                xmlNode = xmlDoc.DocumentElement.SelectSingleNode("SourceSheet");
                sourceSheet = xmlNode.Attributes.GetNamedItem("name").Value;

                xmlNode = xmlDoc.DocumentElement.SelectSingleNode("problem_file_column_name");
                problem_file_column_name = xmlNode.Attributes.GetNamedItem("name").Value;

                xmlNode = xmlDoc.DocumentElement.SelectSingleNode("problem_file_path_name");
                problem_file_path_name = xmlNode.Attributes.GetNamedItem("name").Value;
                

                if (problem_file_column_name.Contains("."))
                {
                    MessageBox.Show("c_file_column_name or h_file_column_name error: Please remove any \".\"  inside any xls file column name, OLE driver doesn't understand it", "Configuration Error");
                    return;
                }


                //--------------------

                xmlNode = xmlDoc.DocumentElement.SelectSingleNode("workFolder");
                workFolder = xmlNode.Attributes.GetNamedItem("path").Value;
                if (workFolder == ".") { workFolder = Directory.GetCurrentDirectory(); }

                xmlNode = xmlDoc.DocumentElement.SelectSingleNode("destFolder");
                destFolder = xmlNode.Attributes.GetNamedItem("path").Value;

                /*
                xmlNode = xmlDoc.DocumentElement.SelectSingleNode("problems");
                XmlNodeList xmlList;
                xmlList = xmlNode.SelectNodes("problem");

                foreach (XmlNode xmlNodeS in xmlList)
                {
                    problemsNames.Add(xmlNodeS.Attributes.GetNamedItem("name").Value);
                }
                */
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Bad Configuration File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                Console.WriteLine("\nOutput folder: " + destFolder);
                Directory.CreateDirectory(destFolder);

                Console.Write("Do you want to generate:\n\t1- Autograde-ready folders\n\t2- Moss-ready folders\n\t3- Both\nPlease enter choice (1, 2 or 3):  ");
                var choice = Console.ReadLine();

                if (int.Parse(choice) == 1)
                {
                    Directory.CreateDirectory(destFolder + "\\Autograde_Ready");
                    generate_files_from_xls(xlsFileName, sourceSheet, problem_file_column_name, problem_file_path_name 
                        , workFolder, destFolder + "\\Autograde_Ready", problemsNames);
                }
                else if (int.Parse(choice) == 2)
                {
                    Directory.CreateDirectory(destFolder + "\\Moss_Ready");
                    generate_moss_files_from_xls(xlsFileName, sourceSheet, problem_file_column_name
                        , destFolder + "\\Moss_Ready");
                }
                else if (int.Parse(choice) == 3)
                {
                    Directory.CreateDirectory(destFolder + "\\Moss_Ready");
                    generate_moss_files_from_xls(xlsFileName, sourceSheet, problem_file_column_name, destFolder+"\\Moss_Ready");

                    Directory.CreateDirectory(destFolder + "\\Autograde_Ready");
                    generate_files_from_xls(xlsFileName, sourceSheet, problem_file_column_name, problem_file_path_name
                        , workFolder, destFolder+"\\Autograde_Ready", problemsNames);
                }
                else
                {
                    Console.WriteLine("Wrong Choice");
                    return;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, e.Source);
                MessageBox.Show(e.StackTrace, e.Message);
            }

        }

        /*
        static void CreateSubFolders(string folderPath, int numOfSections, string[] problemsNames)
        {
            if (!folderPath.EndsWith("\\"))
            {
                folderPath+= "\\";
            }
            for (int i = 0; i < numOfSections; i++)
			{
                Directory.CreateDirectory(folderPath + (i + 1).ToString());
                for (int j = 0; j < problemsNames.Length; j++)
                {
                    Directory.CreateDirectory(folderPath + (i + 1).ToString() + "\\" + problemsNames[j]);
                }
			}
            
        }
         * */

        static void generate_files_from_xls(string fileName, string sourceSheet, string c_file_column_name, string probPathName, string workFolder, string destPath, List<string> problemsNames)
        {
            try
            {
                if (!destPath.EndsWith("\\"))
                {
                    destPath += "\\";
                }

                string strConn = string.Empty;
                if (fileName.Trim().EndsWith(".xlsx"))
                {
                    strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", fileName);
                }
                else if (fileName.Trim().EndsWith(".xls"))
                {
                    strConn = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";", fileName);
                }

                string studentRecord = String.Empty;
                int nStudents = 0;
                using (OleDbConnection connection = new OleDbConnection(strConn))
                {
                    OleDbCommand command = new OleDbCommand(@"SELECT * FROM [" + sourceSheet + "$]", connection);
                    connection.Open();
                    OleDbDataReader reader = command.ExecuteReader();
                    Console.WriteLine("Generating Folders ");
                    //int[] numOfSolutions = new int[problemsNames.Count]; //was needed in the old Autograde version, but now the new Autograde takes care of the number of problems
                    while (reader.Read())
                    {
                        string command_c = reader[c_file_column_name].ToString();

                        //for (int j = 0; j < problemsNames.Count; j++)
                        {
                            ////Check if there's a code for the current problem
                            //if (command_c != "")
                            {
                                //numOfSolutions[j]++;

                                //string studentFolder = destPath + reader["Student ID"].ToString() + "__" + problemsNames[j];
                                string studentFolder = destPath + reader["Student ID"].ToString();
                                Directory.CreateDirectory(studentFolder);

                                #region copy FOS template folder contents
                                {
                                    string source_dir = workFolder + "\\" + "question_template";
                                    string destination_dir = studentFolder;

                                    // Create subdirectory structure in destination    
                                    foreach (string template_dir in Directory.GetDirectories(source_dir, "*", System.IO.SearchOption.AllDirectories))
                                    {
                                        Directory.CreateDirectory(destination_dir + "\\" + template_dir.Replace(source_dir + "\\", ""));
                                    }

                                    foreach (string template_file_path in Directory.GetFiles(source_dir, "*.*", System.IO.SearchOption.AllDirectories))
                                    {
                                        File.Copy(template_file_path, destination_dir + "\\" + template_file_path.Replace(source_dir + "\\", ""));
                                    }

                                    //copy current problem test files from "tests" folder into student "kern" folder
                                    //string current_test_folder = workFolder + "\\tests\\" + problemsNames[j];
                                    //foreach (string test_file_name in Directory.GetFiles(current_test_folder, "*.*", System.IO.SearchOption.AllDirectories))
                                    //{
                                    //    File.Copy(test_file_name, destination_dir + "\\kern\\" + test_file_name.Replace(current_test_folder + "\\", ""), true);
                                    //}

                                }
                                #endregion

                                //write both .c and .h  files into student folder "kern"
                                StreamWriter sWriter = new StreamWriter(studentFolder + "\\" + probPathName + "\\" + probPathName + ".cs");
                                sWriter.WriteLine(command_c);
                                sWriter.Close();

                                ++nStudents;
                            }
                        }
                        //Console.Write("\rStudents finished so far = {0}", numOfSolutions[0].ToString());
                        Console.Write("\rStudents finished so far = {0}", nStudents.ToString());
                    }

                    Console.WriteLine("\n");
                    //for (int j = 0; j < problemsNames.Length; j++)
                    {
                        //Console.WriteLine(problemsNames[j] + ": " + numOfSolutions[j].ToString());
                        //Console.WriteLine("{0}", numOfSolutions[0].ToString());
                        Console.WriteLine("{0}", nStudents.ToString());
                    }
                    Console.WriteLine("\n Done!");
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, e.Source);
                MessageBox.Show(e.StackTrace, e.Message);
            }

        }

        static void generate_moss_files_from_xls(string fileName, string sourceSheet, string c_file_column_name, string destPath)
        {
            try
            {
                if (!destPath.EndsWith("\\"))
                {
                    destPath += "\\";
                }

                string strConn = string.Empty;
                if (fileName.Trim().EndsWith(".xlsx"))
                {
                    strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", fileName);
                }
                else if (fileName.Trim().EndsWith(".xls"))
                {
                    strConn = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";", fileName);
                }

                string studentRecord = String.Empty;
                using (OleDbConnection connection = new OleDbConnection(strConn))
                {
                    OleDbCommand command = new OleDbCommand(@"SELECT * FROM [" + sourceSheet + "$]", connection);
                    connection.Open();
                    OleDbDataReader reader = command.ExecuteReader();
                    Console.WriteLine("Generating Moss Folders ");

                    int numOfSolutions = 0;
                    while (reader.Read())
                    {
                        string command_c = reader[c_file_column_name].ToString();

                        ++numOfSolutions;
                        string studentFolder = destPath + reader["Student ID"].ToString();
                        Directory.CreateDirectory(studentFolder);

                        //write both .c and .h  files into student folder "kern"
                        StreamWriter sWriter = new StreamWriter(studentFolder + "\\command_prompt.c");
                        sWriter.WriteLine(command_c);
                        sWriter.Close();
                        Console.Write("\rStudents finished so far = {0}", numOfSolutions.ToString());
                    }
                    Console.WriteLine("\n");
                    Console.WriteLine("{0}", numOfSolutions.ToString());
                    Console.WriteLine("\n Done!");
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, e.Source);
                MessageBox.Show(e.StackTrace, e.Message);
            }

        }
    }
}

